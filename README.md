**It works only on X. If you're using Wayland, leave my repo.**  
Install xkbcomp on your system.  
Arch Linux:
```bash
pacman -S xorg-xkbcomp
```
Debian/Ubuntu:
```bash
apt install xkbcomp
```
CentOS/Fedora:
```bash
yum install xorg-x11-xkb-utils
```
Then copy `ru` file to `/usr/share/X11/xkb/symbols`:
```bash
cp ru /usr/share/X11/xkb/symbols/ru
```
Now you can enter this symbols:  
Alt + ф = ѳ / Alt + Ф = Ѳ  
Alt + е = ѣ / Alt + Е = Ѣ  
Alt + и = і / Alt + И = І  
Alt + т = ѵ / Alt + Т = Ѵ
